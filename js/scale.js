/**
 * Created by jaro on 15.10.16.
 */

var screen = {
    "dpi": null,
    "x": null,
    "y": null
};

$(function () {
    console.log("starting scale...");

    var connection = new ReconnectingWebSocket(WEBSOCKETS_SCALE_URL);
    connection.onopen = function () {
        console.log("connected to scale server!");
    };

    connection.onmessage = function (e) {
        var videoPosition = JSON.parse(e.data);


        var video = $("#video");
        video.css("width", videoPosition.width + "px");
        video.css("height", videoPosition.height + "px");
        video.css("left", videoPosition.x + "px");
        video.css("top", videoPosition.y + "px");

        // hide the input form
        $("form").css("display", "none");
    };

    $(window).resize(function () {
        if (!(screen.dpi && screen.x && screen.y)) {
            return false;
        }

        var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

        connection.send(JSON.stringify({
            "dpi": screen.dpi,
            "width": w,
            "height": h,

            "x": screen.x,
            "y": screen.y
        }));
    });

    $("form").submit(function (e) {
        var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);

        screen.dpi = $("#dpi").val();
        screen.x = $("#x").val();
        screen.y = $("#y").val();

        if (!(screen.dpi && screen.x && screen.y)) {
            return false;
        }

        connection.send(JSON.stringify({
            "dpi": screen.dpi,
            "width": w,
            "height": h,

            "x": screen.x,
            "y": screen.y
        }));
        console.log("submit");
        return false;  // prevent form from reloading the page
    });


});
