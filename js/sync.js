/**
 * Created by jaro on 15.10.16.
 */


// this variable can be set for simulating a network delay in [ms]
var NETLAG = 0;

$(function () {
    console.log("starting sync...");

    var connection = new ReconnectingWebSocket(WEBSOCKETS_SYNC_URL);
    connection.onopen = function () {
        console.log("connected to sync server!");

    };

    connection.onmessage = function (e) {
        setTimeout(function () {
            handlePacket(e.data);
        }, (NETLAG / 2));
    };

    function send(message) {
        setTimeout(function () {
            connection.send(message)
        }, NETLAG / 2);
    }

    function handlePacket(packet) {
        var type = packet.substr(0, 1);
        var payload = packet.substr(1);

        switch (type) {
            case "p":  // ping
                send("p" + payload);
                break;
            case "s":  // sync
                var videoTime = parseFloat(payload);
                var video = document.getElementById("video");

                var diff = videoTime - video.currentTime;
                console.log(diff);

                if (Math.abs(diff) < 1) {
                    video.playbackRate = 1 + diff;
                } else {
                    video.currentTime = videoTime;
                }
                video.play();

                break;
            default:
                console.error("unknown message type :(\n Message is: " + packet);
        }
    }
});
