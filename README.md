# Videowall frontend
This is the frontend of the Videwall project. One day, it should be able to display
videos distributed and in sync over multiple devices and Screens. The Frontend should be able to answer
Ping requests from Server and sync a Video to ingoing timestamps. Moreover it should be able to display
only a certain region of the Video.